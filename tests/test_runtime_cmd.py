import os
import os.path
import sys
import unittest
import uuid

from d3m import runtime, utils, index

sys.path.insert(0, os.path.dirname(__file__))

from local_test_primitives.dataset_to_dataframe import DatasetToDataFramePrimitive
from local_test_primitives.random_classifier import RandomClassifier

PROBLEM_DIR = os.path.join(os.path.dirname(__file__), 'data', 'problems')
DATASET_DIR = os.path.join(os.path.dirname(__file__), 'data', 'datasets')
PIPELINE_DIR = os.path.join(os.path.dirname(__file__), 'data', 'pipelines')


class TestRuntimeCmd(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        # To hide any logging or stdout output.
        with utils.silence():
            index.register_primitive('d3m.primitives.data_transformation.dataset_to_dataframe.Test',
                                     DatasetToDataFramePrimitive)
            index.register_primitive('d3m.primitives.classification.random_forest.Test', RandomClassifier)

        cwd = os.path.split(os.path.realpath(__file__))[0]
        cls.save_directory = os.path.join(cwd, 'test_pipeline_runs_{}'.format(uuid.uuid4()))

    def test_multi_input_fit(self):
        arg = [
            '',
            'fit',
            '-i', os.path.join(DATASET_DIR, 'iris_dataset_1/datasetDoc.json'),
            '-i', os.path.join(DATASET_DIR, 'iris_dataset_1/datasetDoc.json'),
            '-r', os.path.join(PROBLEM_DIR, 'iris_problem_1/problemDoc.json'),
            '-p', os.path.join(PIPELINE_DIR, 'multi-input-test.json'),
        ]
        with utils.silence():
            runtime.main(arg)

    def test_multi_input_produce(self):
        arg = [
            '',
            'fit-produce',
            '-i', os.path.join(DATASET_DIR, 'iris_dataset_1/datasetDoc.json'),
            '-i', os.path.join(DATASET_DIR, 'iris_dataset_1/datasetDoc.json'),
            '-r', os.path.join(PROBLEM_DIR, 'iris_problem_1/problemDoc.json'),
            '-t', os.path.join(DATASET_DIR, 'iris_dataset_1/datasetDoc.json'),
            '-t', os.path.join(DATASET_DIR, 'iris_dataset_1/datasetDoc.json'),
            '-p', os.path.join(PIPELINE_DIR, 'multi-input-test.json'),
        ]
        with utils.silence():
            runtime.main(arg)
